# Project about online user prediction, IS

## data_prep.py
Получает статитику http запросов пользователей, сохраняет в файл users_statistics.  
>     """  
>     SQL QUERY for user statistics [December, 2017]:   
>         SELECT SUM(event_count)   
>         FROM bigbro.http_req   
>         WHERE user_id = { USER ID }  
>             AND (toDate(event_ts) >= '2017-12-01' AND toDate(event_ts) <= '2018-01-01')   
>             AND toDayOfWeek(event_ts) = { DAY OF THE WEEK }   
>             AND toHour(event_ts) = { HOUR OF THE DAY }  
>     """  
  
Данные сохраняются в виде словаря:  
> { USER_ID : {  
>     1 : { 0:count_of_request, ..., 23:count_of_request },  
>     2 : { 0:count_of_request, ..., 23:count_of_request },  
>     ...  
>     7 : { 0:count_of_request, ..., 23:count_of_request },  
>     },  
> }  
  
## data_show.py.ipynb
Jupyter-блокнот анализа:  
  
Функция отрисовки распределения запросов/вероятностей  
1) Рисует диаграмму распределения запросов пользователя  
2) Рисует диаграммы распределения вероятностей доступа в Интернет (по дням и по часам)  
3) Рисует столбчатую диаграмму запросов пользователя в конкретную дату с указанием следующих вероятностей для каждого часа: по дням, по часам, суммарная  

